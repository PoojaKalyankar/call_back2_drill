function boardInformationInList(list, boardIDs, callback) {
    try {
        setTimeout(() => {
            let listIdInformation = Object.entries(list).filter(listData => listData[0] === boardIDs);
            callback(null, Object.fromEntries(listIdInformation));
        }, 2000);
    } catch (error) {
        console.error(error);
    }
}

module.exports = boardInformationInList;