const boardsInformation = require('./callback1.cjs');
const boardInformationInList = require('./callback2.cjs');
const getListOfCard = require('./callback3.cjs');
let board = require('./boardsid.json');
let list = require('./lists.json');
let cards = require('./cards.json');

function thamousInformation(personName) {
    try {
        boardsInformation(board, "mcu453ed", (error, data) => {
            if (error) {
                console.error(error);
            } else {
                let name = data.filter(identity => identity.name == personName);
                console.log(data);
                console.log("board data of Thanos")
                boardInformationInList(list, name[0].id, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(data)
                        console.log("list data of Thanos")
                        let array = (Object.entries(data))
                        const answerObject = array[0][1].reduce((accumulator, currentObject) => {
                            if (currentObject.name === 'Mind' || currentObject.name === 'Space') {
                                if (!accumulator.includes(currentObject.id)) {
                                    accumulator.push(currentObject.id);
                                }
                            }
                            return accumulator
                        }, [])
                        answerObject.forEach(element => {
                            getListOfCard(cards, element, (error, data) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    console.log(data);
                                    console.log("cards data of mind and space")
                                }

                            })
                        });

                    }
                })
            }
        })
    } catch (error) {
        console.error(error);
    }
}
module.exports = thamousInformation;