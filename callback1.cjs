function boardsInformation(board, boardId, callback) {
   try {
      setTimeout(() => {
         const answer = board.filter((identity) => identity.id === boardId);
         callback(null, answer)
      }, 2000);
   } catch (error) {
      console.error(error);
   }
}

module.exports = boardsInformation;