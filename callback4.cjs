const boardsInformation = require('./callback1.cjs');
const boardInformationInList = require('./callback2.cjs');
const getListOfCard = require('./callback3.cjs');
let board = require('./boardsid.json');
let list = require('./lists.json');
let cards = require('./cards.json');

function thanosInformation(personName) {
    try {
        boardsInformation(board, "mcu453ed", (error, data) => {
            if (error) {
                console.error(error);
            } else {
                let name = data.filter(identity => identity.name == personName);
                console.log(data);
                console.log("board data of Thanos")
                boardInformationInList(list, name[0].id, (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(data)
                        console.log("list data of Thanos")
                        let array = (Object.entries(data))
                        const answerObject = array[0][1].filter(element => element.name === "Mind")
                        let idOfMind = (answerObject[0].id)
                        getListOfCard(cards, idOfMind, (error, data) => {
                            if (error) {
                                console.error(error);
                            } else {
                                console.log(data);
                                console.log("cards data of minds")
                            }
                        })
                    }
                })
            }
        })
    } catch (error) {
        console.error(error);
    }
}

module.exports = thanosInformation;